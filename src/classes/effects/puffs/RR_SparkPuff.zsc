//===========================================================================
//
// RR_SparkPuff
//
// Implements a bullet puff represented by a group of sparks.
//
//===========================================================================
class RR_SparkPuff : RR_EffectBase
{
    // The minimum and maximum number of spawned sparks.
    private int _sparkCountMin, _sparkCountMax;

    // The minimum and maximum lifespan of spawned sparks.
    private double _sparkLifeSpanMin, _sparkLifeSpanMax;

    // The minimum and maximum scale factor of spawned sparks.
    private double _sparkScaleFactorMin, _sparkScaleFactorMax;

    // The minimum and maximum speed of spawned sparks.
    private double _sparkSpeedMin, _sparkSpeedMax;

    property SparkCount : _sparkCountMin, _sparkCountMax;
    property SparkLifeSpan : _sparkLifeSpanMin, _sparkLifeSpanMax;
    property SparkScaleFactor : _sparkScaleFactorMin, _sparkScaleFactorMax;
    property SparkSpeed : _sparkSpeedMin, _sparkSpeedMax;

    Default
    {
        +PUFFGETSOWNER;
        +HITTRACER;

        RR_SparkPuff.SparkCount 1, 1;
        RR_SparkPuff.SparkLifeSpan 1.0, 1.0;
        RR_SparkPuff.SparkScaleFactor 1.0, 1.0;
        RR_SparkPuff.SparkSPeed 1.0, 1.0;
    }

    //===========================================================================
    //
    // RR_SparkPuff::PostBeginPlay
    //
    // Spawns sparks at this actor's position. The number of sparks,
    // their lifespan and speed are determined by the values
    // of the corresponding properties.
    //
    //===========================================================================
    override void PostBeginPlay()
    {
        Super.PostBeginPlay();

        // The PUFFGETSOWNER flag ensures that we have a target,
        // but we still should check if it exists.
        // We also need to check that we either didn't hit anything,
        // or we hit a non-bleeding actor, otherwise we don't spawn sparks.
        if (target && (!tracer || tracer.bNoBlood || tracer.bDormant || tracer.bInvulnerable))
        {
            // We just pass all parameters to RR_Spark::SpawnManyAt
            // which does the actual spawning.
            RR_Spark.SpawnManyAt(
                self, target,
                random[RR_SparkPuff__PostBeginPlay](_sparkCountMin, _sparkCountMax),
                _sparkLifeSpanMin, _sparkLifeSpanMax,
                _sparkScaleFactorMin, _sparkScaleFactorMax,
                _sparkSpeedMin, _sparkSpeedMax
            );
        }

        // We shouldn't stick around any longer once we've done our job.
        Destroy();
    }
}
