//===========================================================================
//
// RR_HudWidgetPowerups
//
// Contains the HUD code responsible for drawing active powerups.
//
//===========================================================================
class RR_HudWidgetPowerups : RR_HudWidgetBase
{
    //===========================================================================
    //
    // RR_HudWidgetPowerups::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_HudWidgetPowerups Create()
    {
        return new('RR_HudWidgetPowerups');
    }

    //===========================================================================
    //
    // RR_HudWidgetPowerups::Draw
    //
    // Draws all active powerups.
    //
    //===========================================================================
    override bool Draw(RR_HudDrawer drawer)
    {
        bool result = false;

        // There used to be a HELL of a lot of redundant code
        // back in SBARINFO times...
        // But have no fear! The future is finally here!

        for (let i = drawer.GetPlayer().mo.Inv; i != NULL; i = i.Inv)
        {
            if (i is 'Powerup')
            {
                result = DrawPowerup(drawer, Powerup(i)) || result;
            }
        }

        return result;
    }

    //===========================================================================
    //
    // RR_HudWidgetPowerups::DrawPowerup
    //
    // Draws the icon and the amount of time remaining for a powerup.
    //
    //===========================================================================
    private static bool DrawPowerup(RR_HudDrawer drawer, Powerup powerup)
    {
        // The icon to draw.
        TextureID icon;
        // Draw color for the time counter.
        int drawColor;
        // Alpha value for the icon and the time counter;
        double alpha;
        // Is the powerup visible in the HUD?
        bool isVisible;
        // Is the time counter of the powerup visible in the HUD?
        bool isTimeVisible;

        // Get all this stuff.
        [icon, drawColor, alpha, isVisible, isTimeVisible] = GetPowerupDrawParameters(drawer, powerup);

        // Only proceed if a valid icon has been found and the powerup is visible.
        if (icon.IsValid() && isVisible)
        {
            drawer.AddRow(20, 2);
            drawer.DrawTexture(icon, (20, 20), alpha);
            // Draw powerup time if it is visible.
            if (isTimeVisible)
            {
                drawer.Step(4);
                drawer.DrawNumber(RR_DNFONT_BIG, RR_Math.TicsToSeconds(powerup.EffectTics), drawColor, alpha);
            }

            return true;
        }

        return false;
    }

    //===========================================================================
    //
    // RR_HudWidgetPowerups::GetPowerupDrawParameters
    //
    // Returns the following parameters for the powerup:
    // - Icon to draw
    // - Time color
    // - Draw alpha
    // - Should the powerup be drawn right now?
    // - Should time be drawn right now?
    //
    //===========================================================================
    private static TextureID, int, double, bool, bool GetPowerupDrawParameters(RR_HudDrawer drawer, Powerup powerup)
    {
        let icon = powerup.GetPowerupIcon();
        name timeColor = 'None';
        double alpha = 1;
        bool isVisible;
        bool isTimeVisible;

        // If the icon is not valid, try a known icon and color.
        if (!drawer.IsValidTexture(icon))
        {
            string knownImage;
            [knownImage, timeColor] = GetKnownPowerupImageAndColor(powerup);

            if (knownImage != "")
            {
                // Known icon found, use default parameters.
                icon = drawer.GetTextureByName(knownImage);
            }
        }

        // Check if the powerup is an RR_Powerup.
        // Then we can get its HUD draw parameters.
        let p = RR_Powerup(powerup);
        if (p)
        {
            timeColor = p.GetHUDColor();
            alpha = p.GetHUDAlpha();
            isVisible = p.IsVisibleInHUD();
            isTimeVisible = p.IsTimeVisibleInHUD();
        }
        else
        {
            // Otherwise, use default parameters.
            alpha = 1;
            isVisible = true;

            // For Berserk, time is never visible since it is effectively infinite.
            // For other powerups, time is visible unless the powerup is blinking.
            isTimeVisible = powerup.GetClassName() == 'PowerStrength' ? false : !powerup.IsBlinking();
        }

        return icon, RR_Hud.GetColorOrDefault(timeColor, Font.CR_GREY), alpha, isVisible, isTimeVisible;
    }

    //===========================================================================
    //
    // RR_HudWidgetPowerups::GetKnownPowerupImageAndColor
    //
    // Returns the image and color for a known powerup.
    //
    //===========================================================================
    private static string, name GetKnownPowerupImageAndColor(Powerup powerup)
    {
        let powerupName = powerup.GetClassName();

        // Invulnerability.
        if (powerupName == 'PowerInvulnerable')
        {
            return "PINVA0", 'DarkGreen';
        }

        // Invisibility (blur sphere).
        if (powerupName == 'PowerInvisibility')
        {
            return "PINSA0", 'Red';
        }

        // Radiation suit.
        if (powerupName == 'PowerIronFeet')
        {
            return "SUITA0", 'Green';
        }

        // Light amp.
        if (powerupName == 'PowerLightAmp')
        {
            return "PVISA0", 'LightBlue';
        }

        // Berserk. Note that color is not required,
        // since the powerup is effectively infinite
        // and we don't draw its remaining time.
        if (powerupName == 'PowerStrength')
        {
            return "PSTRA0", 'None';
        }

        // We don't know what it is.
        return "", 'None';
    }
}
