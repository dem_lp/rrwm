//===========================================================================
//
// RR_HudWidgetStats
//
// Contains the HUD code responsible for drawing level stats.
//
//===========================================================================
class RR_HudWidgetStats : RR_HudWidgetBase
{
    // The color used to draw the stat counters.
    const STATS_COLOR = Font.CR_GREY;

    // List of all counters to display.
    private RR_StatCounterData _counters[3];

    // Number of counters to display.
    private int _counterCount;

    //===========================================================================
    //
    // RR_HudWidgetStats::Create
    //
    // Creates a new instance of this class.
    //
    //===========================================================================
    static RR_HudWidgetStats Create()
    {
        return new('RR_HudWidgetStats');
    }

    //===========================================================================
    //
    // RR_HudWidgetStats::Draw
    //
    // Draws the current level stats (kills, items, secrets).
    //
    //===========================================================================
    override bool Draw(RR_HudDrawer drawer)
    {
        InitCounters();
        return DrawCounters(drawer);
    }

    //===========================================================================
    //
    // RR_HudWidgetStats::InitCounters
    //
    // Initializes all counters.
    //
    //===========================================================================
    private void InitCounters()
    {
        // Reset the number of counters.
        _counterCount = 0;

        // Try to initialize all of the counters.
        TryAddCounter(RR_SETTING_HUD_STAT_KILLS, Level.killed_monsters, Level.total_monsters, "$RR_HUD_STAT_KILLS");
        TryAddCounter(RR_SETTING_HUD_STAT_ITEMS, Level.found_items, Level.total_items, "$RR_HUD_STAT_ITEMS");
        TryAddCounter(RR_SETTING_HUD_STAT_SECRETS, Level.found_secrets, Level.total_secrets, "$RR_HUD_STAT_SECRETS");
    }

    //===========================================================================
    //
    // RR_HudWidgetStats::TryAddCounter
    //
    // Adds a counters to the list if it is currently enabled.
    //
    //===========================================================================
    private void TryAddCounter(E_RR_SettingIndex counterSetting, int curValue, int maxValue, string description)
    {
        // Get the value of the setting.
        int settingValue = RR_Settings.GetValue_Ui(counterSetting);

        // Check if this counter is currently enabled.
        if (settingValue == RR_OPTVAL_OFF || settingValue == RR_OPTVAL_IF_NONZERO && curValue == 0 && maxValue == 0)
        {
            return;
        }

        int i = _counterCount;
        _counters[i].CurValue = curValue;
        _counters[i].MaxValue = maxValue;
        _counters[i].Description = StringTable.Localize(description);

        // We have one more counter now.
        _counterCount++;
    }

    //===========================================================================
    //
    // RR_HudWidgetStats::DrawCounters
    //
    // Draws all the counters that are currently enabled.
    //
    //===========================================================================
    private bool DrawCounters(RR_HudDrawer drawer)
    {
        int digitsCur = 0, digitsMax = 0, maxDescLength = 0;

        // Determine the parameters for drawing.
        for (int i = 0; i < _counterCount; i++)
        {
            digitsCur = Max(digitsCur, RR_Math.GetDigits(_counters[i].CurValue));
            digitsMax = Max(digitsMax, RR_Math.GetDigits(_counters[i].MaxValue));
            maxDescLength = Max(maxDescLength, _counters[i].Description.CodePointCount());
        }

        // Now do the actual drawing.
        // NB! Drawing must be done in reverse order (since it is bottom to top).
        for (int i = _counterCount - 1; i >= 0; i--)
        {
            drawer.AddRow(10, 2);
            drawer.DrawString(RR_DSFONT_SMALL, _counters[i].Description .. ":", STATS_COLOR, minLength: maxDescLength + 1);
            drawer.DrawNumber(RR_DNFONT_SMALL, _counters[i].CurValue, STATS_COLOR, minDigits: digitsCur);
            drawer.Step(2);
            drawer.DrawSlash(RR_DSFONT_SMALL, STATS_COLOR);
            drawer.Step(2);
            drawer.DrawNumber(RR_DNFONT_SMALL, _counters[i].MaxValue, STATS_COLOR, minDigits: digitsMax);
        }

        return _counterCount > 0;
    }
}
