//===========================================================================
//
// RR_DoomStatusBar
//
// Re-implements the Doom status bar with RRWM-specific features.
//
// Unfortunatly, the DoomStatusBar class is poorly extendable,
// so we have to write our own implementation.
//
//===========================================================================
class RR_DoomStatusBar ui
{
    // The main font to draw big numbers with.
    private HUDFont _HUDFont;
    // The font used to draw values in the ammo table.
    private HUDFont _indexFont;
    // The font used when drawing amounts in the inventory bar.
    private HUDFont _amountFont;
    // Stores information about the inventory bar state.
    private InventoryBarState _diparms;

    // The base status bar class this status bar operates upon.
    private BaseStatusBar _statusBar;

    //===========================================================================
    //
    // RR_DoomStatusBar::Create
    //
    // Creates and initializes a new instance of this class.
    //
    //===========================================================================
    static RR_DoomStatusBar Create(BaseStatusBar statusBar)
    {
        let doomStatusBar = new('RR_DoomStatusBar');
        doomStatusBar.Init(statusBar);

        return doomStatusBar;
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::Init
    //
    // Initializes the necessary parameters to draw the Doom status bar.
    //
    //===========================================================================
    private void Init(BaseStatusBar statusBar)
    {
        _statusBar = statusBar;
        _statusBar.SetSize(32, 320, 200);

        // Create the font used for the fullscreen HUD
        Font fnt = "HUDFont_DOOM";
        _HUDFont = HUDFont.Create(fnt, fnt.GetCharWidth("0"), true, 1, 1);
        fnt = "INDEXFONT_DOOM";
        _indexFont = HUDFont.Create(fnt, fnt.GetCharWidth("0"), true);
        _amountFont = HUDFont.Create("INDEXFONT");
        _diparms = InventoryBarState.Create();
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::Draw
    //
    // Draws the Doom status bar.
    //
    //===========================================================================
    void Draw()
    {
        let player = _statusBar.CPlayer;

        DrawImage("STBAR", (0, 168));
        DrawImage("STTPRCNT", (90, 171));
        DrawImage("STTPRCNT", (221, 171));

        Inventory a1 = _statusBar.GetCurrentAmmo();
        if (a1)
        {
            // Draw ammo amount if the weapon is not reloadable, otherwise, draw clip amount.
            // Unfortunatly, it seems it isn't possible to scale the strings we draw,
            // which makes it impossible to shrink the size a bit and add the main amount underneath
            // in SMALLFONT.
            int amount = a1.Amount;
            let weapon = RR_ReloadableWeapon(player.ReadyWeapon);
            if (weapon)
            {
                amount = weapon.GetClipAmount();
            }
            DrawString(_HUDFont, FormatNumber(amount, 3), (44, 171), false);
        }
        DrawString(_HUDFont, FormatNumber(player.health, 3), (90, 171), false);
        DrawString(_HUDFont, FormatNumber(statusBar.GetArmorAmount(), 3), (221, 171), false);

        bool locks[6];
        String image;
        for(int i = 0; i < 6; i++) locks[i] = player.mo.CheckKeys(i + 1, false, true);
        // key 1
        if (locks[1] && locks[4]) image = "STKEYS6";
        else if (locks[1]) image = "STKEYS0";
        else if (locks[4]) image = "STKEYS3";
        DrawImage(image, (239, 171));
        // key 2
        if (locks[2] && locks[5]) image = "STKEYS7";
        else if (locks[2]) image = "STKEYS1";
        else if (locks[5]) image = "STKEYS4";
        else image = "";
        DrawImage(image, (239, 181));
        // key 3
        if (locks[0] && locks[3]) image = "STKEYS8";
        else if (locks[0]) image = "STKEYS2";
        else if (locks[3]) image = "STKEYS5";
        else image = "";
        DrawImage(image, (239, 191));

        int amt1, maxamt;
        [amt1, maxamt] = _statusBar.GetAmount("RR_Clip");
        DrawString(_indexFont, FormatNumber(amt1, 3), (288, 173));
        DrawString(_indexFont, FormatNumber(maxamt, 3), (314, 173));

        [amt1, maxamt] = statusBar.GetAmount("RR_Shell");
        DrawString(_indexFont, FormatNumber(amt1, 3), (288, 179));
        DrawString(_indexFont, FormatNumber(maxamt, 3), (314, 179));

        [amt1, maxamt] = statusBar.GetAmount("RR_RocketAmmo");
        DrawString(_indexFont, FormatNumber(amt1, 3), (288, 185));
        DrawString(_indexFont, FormatNumber(maxamt, 3), (314, 185));

        [amt1, maxamt] = statusBar.GetAmount("RR_Cell");
        DrawString(_indexFont, FormatNumber(amt1, 3), (288, 191));
        DrawString(_indexFont, FormatNumber(maxamt, 3), (314, 191));

        if (deathmatch || teamplay)
        {
            DrawString(_HUDFont, FormatNumber(player.FragCount, 3), (138, 171));
        }
        else
        {
            DrawImage("STARMS", (104, 168));
            DrawImage(player.HasWeaponsInSlot(2)? "STYSNUM2" : "STGNUM2", (111, 172));
            DrawImage(player.HasWeaponsInSlot(3)? "STYSNUM3" : "STGNUM3", (123, 172));
            DrawImage(player.HasWeaponsInSlot(4)? "STYSNUM4" : "STGNUM4", (135, 172));
            DrawImage(player.HasWeaponsInSlot(5)? "STYSNUM5" : "STGNUM5", (111, 182));
            DrawImage(player.HasWeaponsInSlot(6)? "STYSNUM6" : "STGNUM6", (123, 182));
            DrawImage(player.HasWeaponsInSlot(7)? "STYSNUM7" : "STGNUM7", (135, 182));
        }

        if (multiplayer)
        {
            DrawImage("STFBANY", (143, 168), true);
        }

        if (player.mo.InvSel && !Level.NoInventoryBar)
        {
            _statusBar.DrawInventoryIcon(player.mo.InvSel, (160, 198));
            if (player.mo.InvSel.Amount > 1)
            {
                DrawString(_amountFont, FormatNumber(player.mo.InvSel.Amount), (175, 198-_indexFont.mFont.GetHeight()), color: Font.CR_GOLD);
            }
        }
        else
        {
            DrawTexture(_statusBar.GetMugShot(5), (143, 168));
        }
        if (_statusBar.isInventoryBarVisible())
        {
            DrawInventoryBar((48, 169), 7);
        }
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawString
    //
    // Implements the DrawString method for the Doom status bar.
    //
    //===========================================================================
    private void DrawString(HUDFont drawFont, string what, vector2 where, bool shadow = true, int color = Font.CR_UNTRANSLATED)
    {
        _statusBar.DrawString(drawFont, what, where, BaseStatusBar.DI_TEXT_ALIGN_RIGHT | (shadow ? 0 : BaseStatusBar.DI_NOSHADOW), color);
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::FormatNumber
    //
    // Implements the FormatNumber method for the Doom status bar.
    //
    //===========================================================================
    private string FormatNumber(int value, int minDigits = 0)
    {
        return BaseStatusBar.FormatNumber(value, minDigits);
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawImage
    //
    // Implements the DrawImage method for the Doom status bar.
    //
    //===========================================================================
    private void DrawImage(string image, vector2 where, bool translatable = false)
    {
        _statusBar.DrawImage(image, where, BaseStatusBar.DI_ITEM_OFFSETS | (translatable ? BaseStatusBar.DI_TRANSLATABLE : 0));
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawTexture
    //
    // Implements the DrawTexture method for the Doom status bar.
    //
    //===========================================================================
    private void DrawTexture(TextureID texture, vector2 where)
    {
        _statusBar.DrawTexture(texture, (143, 168), BaseStatusBar.DI_ITEM_OFFSETS);
    }

    //===========================================================================
    //
    // RR_DoomStatusBar::DrawInventoryBar
    //
    // Implements the DrawInventoryBar method for the Doom status bar.
    //
    //===========================================================================
    private void DrawInventoryBar(vector2 where, int numfields)
    {
        _statusBar.DrawInventoryBar(_diparms, where, numfields, BaseStatusBar.DI_ITEM_LEFT_TOP);
    }
}
