//===========================================================================
//
// RR_Math
//
// Contains some helper math functions
//
//===========================================================================
class RR_Math
{
    // This is used to compare floating point numbers
    const EPS = 1e-5;

    //===========================================================================
    //
    // RR_Math::AreAlmostEqual
    //
    // Checks if two values are almost equal to each other.
    //
    //===========================================================================
    static bool AreAlmostEqual(double x, double y)
    {
        return Abs(x - y) < EPS;
    }

    //===========================================================================
    //
    // RR_Math::AreAlmostEqualV
    //
    // Checks if two vectors are almost equal to each other.
    //
    //===========================================================================
    static bool AreAlmostEqualV(vector3 v1, vector3 v2)
    {
        return AreAlmostEqual(v1.x, v2.x)
            && AreAlmostEqual(v1.y, v2.y)
            && AreAlmostEqual(v1.z, v2.z);
    }

    //===========================================================================
    //
    // RR_Math::BoolToPosNeg
    //
    // Returns 1 if the value is true and -1 if the value is false.
    //
    //===========================================================================
    static int BoolToPosNeg(bool value)
    {
        return (value << 1) - 1;
    }

    //===========================================================================
    //
    // RR_Math::GetDigits
    //
    // Returns the number of digits in an integer value.
    //
    //===========================================================================
    static int GetDigits(int number)
    {
        int x = 1;
        if (number < 0)
        {
            number = -number;
            x++;
        }

        while (number >= 10)
        {
            number /= 10;
            x++;
        }

        return x;
    }

    //===========================================================================
    //
    // RR_Math::GetNonZeroValue
    //
    // Returns the first value if it's non-zero,
    // otherwise returns the second value.
    //
    //===========================================================================
    static double GetNonZeroValue(double value, double valueIfZero)
    {
        return IsAlmostZero(value) ? valueIfZero : value;
    }

    //===========================================================================
    //
    // RR_Math::GetPitchFromVector
    //
    // Calculates the pitch value of the vector.
    //
    //===========================================================================
    static double GetPitchFromVector(vector3 v)
    {
        double len = v.Length();
        if (IsAlmostZero(len))
        {
            // In this case, the pitch is not really defined,
            // but for simplicity's sake we assume it's 0.
            return 0;
        }

        return -Asin(v.Z / len);
    }

    //===========================================================================
    //
    // RR_Math::GetPosAndVelForWeaponObject
    //
    // Given the initial offset in 2D, velocity and its spread,
    // returns initial position in 3D and a randomized velocity
    // for an object to be spawned from an actor's weapon,
    // in absolute coordinates.
    //
    //===========================================================================
    static vector3, vector3 GetPosAndVelForWeaponObject(
        Actor source, double radiusFactor, double spawnHeight,
        vector2 spawnOffset, vector3 spawnVelocity, vector3 velocitySpread)
    {
        // Calculate the position and the velocity.
        // NB! Depending on the pitch, the resulting position, as seen on the screen, will differ.
        // It is also known that it depends on which renderer is used (software v. OpenGL),
        // so attempting to approximate it is probably not worth it.
        let spawnPos = GetOffsetForWeaponObject(spawnOffset, source, radiusFactor);
        spawnPos = OffsetToPosition(spawnPos, source);

        let spawnVel = RandomizeVector(spawnVelocity, velocitySpread);
        spawnVel = GetRotatedOffset(spawnVel, source);

        return (spawnPos.X, spawnPos.Y, spawnPos.Z + spawnHeight - source.FloorClip), spawnVel;
    }

    //===========================================================================
    //
    // RR_Math::GetViewHeight
    //
    // Gets the actual (for players) or the supposed (for monsters)
    // view height of the source actor. Used when spawning debris objects
    // from the source actor's weapon.
    //
    //===========================================================================
    static double GetViewHeight(Actor source)
    {
        let player = source.player;
        // Is it a player? Then get its view height value
        // multiplied by the crouch factor.
        if (player)
        {
            return player.mo.ViewHeight * player.CrouchFactor;
        }

        // Otherwise, calculate the view height proportionally
        // based on the player's default view height.
        let def = GetDefaultByType('RR_Player');
        return source.Height * def.ViewHeight / def.Height;
    }

    //===========================================================================
    //
    // RR_Math::GetRotatedOffset
    //
    // Given a vector in the coordinate system
    // relative to an actor's position, yaw, pitch and roll,
    // transforms it into a vector in the coordinate system
    // relative to just the actor's position (i.e. a position offset).
    //
    // Example purposes of this function:
    //
    // - transform the result to absolute coordinates
    //   to spawn something directly (via Spawn);
    //
    // - use the result as a velocity;
    //
    // - use the result as an offset passed to another spawn function
    //   (i.e. A_SpawnParticle)
    //
    //===========================================================================
    static vector3 GetRotatedOffset(vector3 v, Actor origin)
    {
        return RotateVector(v, origin.Angle, origin.Pitch, origin.Roll);
    }

    //===========================================================================
    //
    // RR_Math::GetOffsetForWeaponObject
    //
    // Given an offset in 2D coordinates, returns a 3D vector
    // representing the position that would make a spawned object
    // appear to have come out of a weapon the actor is carrying.
    // For players, this means the object will be seen on the screen
    // if the Y value is not too low.
    //
    //===========================================================================
    static vector3 GetOffsetForWeaponObject(vector2 v, Actor source, double radiusFactor)
    {
        // Radius may be 0 for ghost monsters.
        // In this case, fall back to actor defaults of the source.
        double radius = GetNonZeroValue(
            source.Radius,
            source.Default.Radius
        );

        // Radius factor is usually 1, only the Spider Mastermind uses a factor of 0.5
        // because its radius is much bigger than it should be.
        radius *= radiusFactor;

        // Make sure we don't exceed the source actor's collision box,
        // lest the object may spawn outside of it and outside of the map geometry.
        double maxDistSquared = radius - 1.5;
        maxDistSquared *= maxDistSquared;
        double vLengthSquared = v.X * v.X + v.Y * v.Y;

        double distance;
        // If the square of the vector's length is bigger than the square of the max distance,
        // we can't ensure the vector will always end up within the bounding box.
        // Therefore, we print a warning and return 0 as the X coordinate.
        if (vLengthSquared > maxDistSquared)
        {
            Console.Printf("RR_Math.GetOffsetForWeaponObject: unable to calculate safe distance (%f > %f)", vLengthSquared, maxDistSquared);
            distance = 0;
        }
        else
        {
            // Otherwise, it's OK and we compute the safe distance.
            distance = Sqrt(maxDistSquared - vLengthSquared);
        }

        return (distance, -v.X, v.Y);
    }

    //===========================================================================
    //
    // RR_Math::OffsetToPosition
    //
    // Same as GetRotatedOffset, but returns absolute coordinates.
    //
    //===========================================================================
    static vector3 OffsetToPosition(vector3 v, Actor origin)
    {
        let offset = GetRotatedOffset(v, origin);
        return origin.Vec3Offset(offset.x, offset.y, offset.z);
    }

    //===========================================================================
    //
    // RR_Math::IsAlmostZero
    //
    // Checks if the value is almost zero.
    //
    //===========================================================================
    static bool IsAlmostZero(double x)
    {
        return AreAlmostEqual(x, 0);
    }

    //===========================================================================
    //
    // RR_Math::IsAlmostZeroV
    //
    // Checks if the vector's length is almost zero.
    //
    //===========================================================================
    static bool IsAlmostZeroV(vector3 v)
    {
        return IsAlmostZero(v.Length());
    }

    //===========================================================================
    //
    // RR_Math::LogisticCurve
    //
    // Returns the value of a logistic function.
    // The function is defined to be almost 0 with x approaching 0.
    // As x approaches mid, the value approaches 0.5, and when x == mid,
    // the value is exactly 0.5. Larges values of x result in values closer to 1.
    //
    //===========================================================================
    static double LogisticCurve(double x, double mid)
    {
        double e = exp(8*(x / mid - 1));
        return 1.0 / (1.0 + 1.0/e);
    }

    //===========================================================================
    //
    // RR_Math::RotateVector
    //
    // This function does the real work for GetRotatedOffset,
    // it can also be used independently if the values needed
    // to transform the vector are not part of an actor.
    //
    //===========================================================================
    static vector3 RotateVector(vector3 v, double yaw, double pitch, double roll)
    {
        double sinAlpha = sin(yaw);
        double cosAlpha = cos(yaw);
        double sinBeta = sin(pitch);
        double cosBeta = cos(pitch);
        double sinGamma = sin(roll);
        double cosGamma = cos(roll);

        // These are the rows of the rotation matrix, not the columns.
        // They are in this form just for readability.

        let a1 = (
            cosAlpha * cosBeta,
            cosAlpha * sinBeta * sinGamma - sinAlpha * cosGamma,
            cosAlpha * sinBeta * cosGamma + sinAlpha * sinGamma
        );

        let a2 = (
            sinAlpha * cosBeta,
            sinAlpha * sinBeta * sinGamma + cosAlpha * cosGamma,
            sinAlpha * sinBeta * cosGamma - cosAlpha * sinGamma
        );

        let a3 = (
           -sinBeta,
           cosBeta * sinGamma,
           cosBeta * cosGamma
        );

        return (v dot a1, v dot a2, v dot a3);
    }

    //===========================================================================
    //
    // RR_Math::RandomizeVector
    //
    // Randomizes the vector by adding a random value
    // generated in the specified ranges to each component.
    //
    //===========================================================================
    static vector3 RandomizeVector(vector3 v, vector3 spread)
    {
        return (
            v.X + frandom[RR_Math__RandomizeVector](-spread.X, spread.X),
            v.Y + frandom[RR_Math__RandomizeVector](-spread.Y, spread.Y),
            v.Z + frandom[RR_Math__RandomizeVector](-spread.Z, spread.Z)
        );
    }

    //===========================================================================
    //
    // RR_Math::RandomRoll
    //
    // Returns true if a random value between 0 and 1 turns out
    // to be less or equal to the provided chance.
    //
    //===========================================================================
    static bool RandomRoll(double chance)
    {
        // frandom's upper bound is exclusive.
        // This has been verified by inspecting GZDoom's sources.
        // Therefore, only one "<" check is required:
        // for 0, it will always fail, and for 1 it will always succeed.
        return frandom[RR_Math__RandomRoll](0, 1) < chance;
    }

    //===========================================================================
    //
    // RR_Math::TicsToSeconds
    //
    // Converts the provided amount of time in tics
    // to an equivalent amount of time in seconds.
    //
    //===========================================================================
    static int TicsToSeconds(int tics)
    {
        return int(Ceil(tics / double(Thinker.TICRATE)));
    }
}
