//===========================================================================
//
// RR_Obituary
//
// Provides a static method to localize and format obituaries.
//
//===========================================================================
class RR_Obituary
{
    //===========================================================================
    //
    // RR_Obituary::Select
    //
    // Selects the correct obituary based on the nature of the source of damage
    // (the object), and also the nature of the killer (the subject).
    //
    //===========================================================================
    static string Select(string obit, Actor obj, Actor subj)
    {
        // obj - the object used to kill the target (i.e. weapon or projectile)
        // subj - the subject who killed the target (i.e. monster or player)

        // If the obituary is non-localizable, skip the entire process.
        if (!IsLocalizablePlaceholder(obit))
        {
            return obit;
        }

        // Store the list of possible matches here.
        // Non-localized matches will be discarded.
        Array<string> matches;

        // Is the subject is a monster?
        bool isMonsterAttack = subj && !subj.player;

        // If the object is a stray projectile, check for the stray obituary
        // and skip everything else.
        if (obj is 'RR_Projectile' && !obj.target)
        {
            matches.Push(StringTable.Localize(obit .. "_STRAY"));
        }
        else
        {
            // Check the normal obituary (for player attacks).
            matches.Push(StringTable.Localize(obit));

            // is the subject a monster?
            if (isMonsterAttack)
            {
                // Check the monster obituary.
                matches.Push(TryLocalizeString(obit .. "_MONSTER"));

                // Is the subject a stealth monster?
                if (subj.bStealth)
                {
                    // Check the stealth monster obituary.
                    matches.Push(TryLocalizeString(obit .. "_MONSTER_STEALTH"));
                }
            }
        }

        // Get the best match by iterating over the array in reverse order.
        // Non-localized matches are skipped.
        int i = matches.Size() - 1;
        while (i > 0 && IsLocalizablePlaceholder(matches[i]))
        {
            i--;
        }

        // This is our result...
        string result = matches[i];

        // ... but if the subject is a monster,
        // also substitute "%k" for its tag.
        // For player attacks, the engine does this for us.
        if (isMonsterAttack)
        {
            result.Replace("%k", subj.GetTag());
        }

        // ...yeah.
        return result;
    }

    //===========================================================================
    //
    // RR_Obituary::TryLocalizeString
    //
    // Tries to localize a string if it is localizable,
    // and returns the localized string on success.
    // If the string cannot be localized, returns the original string instead.
    //
    //===========================================================================
    private static string TryLocalizeString(string str)
    {
        // Check if the string is localizable
        if (IsLocalizablePlaceholder(str))
        {
            // Try localizing the string.
            string localized = StringTable.Localize(str);

            // Check if the string has been localized.
            if (localized != str.Mid(1, str.Length() - 1))
            {
                // Success!
                str = localized;
            }
        }

        return str;
    }

    //===========================================================================
    //
    // RR_Obituary::IsLocalizablePlaceholder
    //
    // Checks if a string is a placeholder intended to be localized.
    //
    //===========================================================================
    private static bool IsLocalizablePlaceholder(string str)
    {
        return str.Length() > 0 && str.ByteAt(0) == "$";
    }
}
