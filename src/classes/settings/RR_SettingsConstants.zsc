//===========================================================================
//
// RR_SettingsConstants
//
// Contains enums and constants for the RRWM settings subsystem.
//
//===========================================================================

// This is the common prefix for all setting-related strings.
// Used to generate a localized string name for language.enu lookup.
const RR_CVAR_PREFIX = "rrwm_";

// The setting indexes correspond to the actual settings and are used
// to quickly access a setting in the settings array, without the need
// to search by a CVAR name.
enum E_RR_SettingIndex
{
    // HUD options
    // OK to change these indices since they are never serialized
    // in save files.
    RR_SETTING_HUD_MUGSHOT             =  0,
    RR_SETTING_HUD_PLAYERICON          =  1,
    RR_SETTING_HUD_STAT_KILLS          =  2,
    RR_SETTING_HUD_STAT_ITEMS          =  3,
    RR_SETTING_HUD_STAT_SECRETS        =  4,
    RR_SETTING_HUD_AMMOTABLE           =  5,
    RR_SETTING_HUD_AMMOTABLE_SHOWALL   =  6,
    RR_SETTING_HUD_AMMOTABLE_SHOWCUR   =  7,
    RR_SETTING_HUD_LOWAMMO             = 23,
    RR_SETTING_HUD_LOWAMMO_THRESHOLD   = 24,
    RR_SETTING_HUD_DEBUG               = 25,

    // User options
    // These indices may only be changed if there are no RR_SettingRef's
    // referencing them.
    RR_SETTING_RELOADMODE              =  8,
    RR_SETTING_AUTORELOAD_THRESHOLD    =  9,

    // Game options
    // These indices may only be changed if there are no RR_SettingRef's
    // referencing them.
    RR_SETTING_DEBRISQUEUE_ENABLED     = 10,
    RR_SETTING_DEBRISQUEUE_LIMIT       = 11,
    RR_SETTING_SNIPING                 = 12,
    RR_SETTING_CASINGSMOKE             = 13,
    RR_SETTING_SPIDERDEMON_CASINGS     = 14,
    RR_SETTING_ROCKETSMOKE             = 15,
    RR_SETTING_CYBERDEMON_NEWROCKETS   = 16,

    // Persistent game options
    // These indices should not be changed,
    // or save compatibility may be broken.
    RR_SETTING_RAILGUN                 = 17,
    RR_SETTING_EXTRAARMOR              = 18,
    RR_SETTING_LIMITBERSERK            = 19,
    RR_SETTING_MONSTERRELOAD           = 20,
    RR_SETTING_NEWCHAINGUNGUY          = 21,
    RR_SETTING_SHOTGUN_REALISTICRELOAD = 22
};

// These are the possible setting scopes.
enum E_RR_SettingScope
{
    // This is a user setting, which is local to the player
    // and can be changed at any time.
    RR_SS_USER,
    // This is a server setting, which affects all players
    // and can only be changed by the host at any time.
    RR_SS_SERVER,
    // This is a persistent server setting, which affects all players
    // and can only be changed by the host.
    // Any changes will only take effect on a new game.
    RR_SS_SERVER_PERSISTENT,

    // Total number of scopes. This is not an actual scope value
    // but a marker to easily get the number of scopes we have.
    RR_NUM_SETTING_SCOPES
};

// Possible values for the gameplay impact parameter
enum E_RR_SettingGameplayImpact
{
    // No impact on gameplay.
    RR_SGI_NONE,
    // Little impact on gameplay.
    RR_SGI_LOW,
    // Medium impact on gameplay.
    RR_SGI_MEDIUM,
    // Sever impact on gameplay.
    RR_SGI_HIGH
};

// Possible values for "reload mode" setting
enum E_RR_ReloadMode
{
    // Full auto: the weapon will reload automatically if its clip is empty,
    // without the need to press the reload button.
    RR_RM_FULLAUTO = 0,
    // Semi-auto: the weapon will reload if attempting to fire it with an empty clip.
    RR_RM_SEMIAUTO = 1,
    // Manual: the weapon will only reload if the reload buttom is pressed.
    RR_RM_MANUAL   = 2
};

// Possible values for common option value sets
enum E_RR_OptionValues
{
    // The option is always off.
    RR_OPTVAL_OFF            = 0,

    // The option is always on.
    RR_OPTVAL_ON             = 1,

    // The option is on if the value it controls is non-zero.
    RR_OPTVAL_IF_NONZERO     = 2,

    // The option is on in multiplayer and off in single player.
    RR_OPTVAL_IN_MULTIPLAYER = 2
}

// Possible comparison types for setting dependencies
enum E_RR_ValueComparisonType
{
    // The actual values must be greated than the desired value.
    RR_VCT_GREATER_THAN = 0,

    // The actual value must be equal to the desired value.
    RR_VCT_EQUALS       = 1
}
