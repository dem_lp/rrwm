//===========================================================================
//
// RR_ShotgunGuy
//
// A version of Doom's Shotgun Guy
// equipped with the RRWM version of the Shotgun.
//
//===========================================================================
class RR_ShotgunGuy : RR_HitscanMonster replaces ShotgunGuy
{
    // Enumerates possible states for the Shotgun Guy reloading sequence
    // state machine.
    enum EReloadSequenceState
    {
        // Normal reloading sequence.
        RSS_Normal,

        // The pumping animation will be shown on the next iteration.
        RSS_PrePump,

        // The pumping animation has completed, resume normal reloading.
        RSS_PostPump
    }

    // Current state of the reloading sequence.
    private EReloadSequenceState _reloadSequenceState;

    Default
    {
        Health 30;
        Radius 20;
        Height 56;
        Mass 100;
        Speed 8;
        PainChance 170;
        +FLOORCLIP;

        SeeSound "shotguy/sight";
        PainSound "shotguy/pain";
        DeathSound "shotguy/death";
        ActiveSound "shotguy/active";

        Tag "$TAG_RR_SHOTGUY";

        DropItem 'RR_Shotgun';
        RR_ReloadingMonster.WeaponType 'RR_Shotgun';
    }

    States
    {
        Spawn:
            SPOS AB 10 A_Look;
            Loop;
        See:
            SPOS AABBCCDD 3 A_ChaseOrReload;
            Loop;
        Missile:
            SPOS E 10 A_FaceTarget;
            SPOS F 10 BRIGHT Light("RR_ZOMBIEATK") A_WeaponAttack;
            SPOS E 2;
            SPOS E 8 A_ShotgunPump1;
            TNT1 A 0 A_ShotgunPump2(true);
            Goto See;
        ReloadWeapon:
            SPOS E 12 A_ShotgunGuyReloadStart;
            // Fall through here
        ReloadRound:
            SPOS E 3 A_ShotgunGuyReload;
            SPOS E 2 A_PlayWeaponAuxSound("rrwm/weapons/shotgun/shell");
            SPOS E 13;
            Loop;
        ReloadDone:
            SPOS E 9;
            SPOS E 8 A_ShotgunPump1;
            SPOS E 15 A_ShotgunPump2(false);
            Goto See;
        Pain:
            SPOS G 3;
            SPOS G 3 A_Pain;
            Goto See;
        Death:
            SPOS H 5;
            SPOS I 5 A_Scream;
            SPOS J 5 A_NoBlocking;
            SPOS K 5;
            SPOS L -1;
            Stop;
        XDeath:
            SPOS M 5;
            SPOS N 5 A_XScream;
            SPOS O 5 A_NoBlocking;
            SPOS PQRST 5;
            SPOS U -1;
            Stop;
        Raise:
            SPOS L 5;
            SPOS KJIH 5;
            Goto See;
        // Extra states added in 1.02
        ReloadContinue:
            SPOS E 9;
            SPOS E 8 A_ShotgunPump1;
            SPOS E 16 A_ShotgunPump2(false);
            Goto ReloadWeapon;
        RealisticReloadDone:
            SPOS E 6;
            Goto See;
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::WeaponAttack
    //
    // Fires the zombie's shotgun.
    //
    //===========================================================================
    override void WeaponAttack()
    {
        HitscanAttack(22.5, 0, 3, random[RR_ShotgunGuy__WeaponAttack](1, 5) * 3);
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ShotgunPump1
    //
    // Plays the first half of the shotgun pumping sound sequence.
    //
    //===========================================================================
    void A_ShotgunPump1()
    {
        A_PlayWeaponAuxSound("rrwm/weapons/shotgun/pump1");
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ShotgunPump2
    //
    // Plays the second half of the shotgun pumping sound sequence.
    // Also ejects a casing if the provided argument's value is true.
    //
    //===========================================================================
    void A_ShotgunPump2(bool ejectCasing)
    {
        A_PlayWeaponAuxSound("rrwm/weapons/shotgun/pump2");
        if (ejectCasing)
        {
            A_EjectCasing();
        }
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ShotgunGuyReloadStart
    //
    // Resets the reload sequence state before initiating a new sequence.
    //
    //===========================================================================
    void A_ShotgunGuyReloadStart()
    {
        _reloadSequenceState = RSS_Normal;
    }

    //===========================================================================
    //
    // RR_ShotgunGuy::A_ShotgunGuyReload
    //
    // Calls A_ReloadOneRound and potentially jumps to different states
    // to emulate the realistic reloading sequence if the corresponding setting
    // is enabled.
    //
    //===========================================================================
    state A_ShotgunGuyReload()
    {
        let clip = GetClip();

        if (clip)
        {
            // Look for targets while we are reloading,
            // but don't jump to any other states.
            // This is needed if the monster is reloading its weapon
            // after the target has died.
            if (!target || target.health <= 0)
            {
                A_LookEx(LOF_NOJUMP);
            }

            // If the pumping animation has just finished playing, skip this part.
            if (_reloadSequenceState != RSS_PostPump)
            {
                // Check if realistic reloading is enabled.
                bool realisticReloadEnabled = RR_Settings.GetValue_Play(RR_SETTING_SHOTGUN_REALISTICRELOAD);

                // Check if we should stop reloading.
                if (ShouldStopReloading())
                {
                    return realisticReloadEnabled && _reloadSequenceState != RSS_PrePump
                        ? ResolveState('RealisticReloadDone')
                        : ResolveState('ReloadDone');
                }

                // If we got here, this means we will load the next round,
                // either immediately or after the pumping animation.

                // Check if we need to play the pumping animation.
                if (_reloadSequenceState == RSS_PrePump)
                {
                    // This ensures that we will always load the next round.
                    _reloadSequenceState = RSS_PostPump;
                    return ResolveState('ReloadContinue');
                }

                // Set the flag to play the pumping animation, if necessary.
                if (realisticReloadEnabled && clip.GetAmount() == 0)
                {
                    _reloadSequenceState = RSS_PrePump;
                }
            }
            else
            {
                // Resume normal reloading.
                _reloadSequenceState = RSS_Normal;
            }

            // Load one round of ammo into the clip.
            clip.LoadAmmo(clip.GetAmmoUse());
        }

        return NULL;
    }
}
