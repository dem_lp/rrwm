//===========================================================================
//
// RR_SpiderMastermind
//
// A variant of Doom's Spider Mastermind which behaves exactly
// like the vanilla one, except that it has a different attack sound
// and it also ejects shotgun casings while firing.
//
//===========================================================================
class RR_SpiderMastermind : SpiderMastermind replaces SpiderMastermind
{
    // The casing type to use.
    private class<RR_CasingBase> _casingType;

    // The puff type to use.
    private class<Actor> _puffType;

    States
    {
        Missile:
            SPID A 20 BRIGHT A_FaceTarget;
            SPID G 1 BRIGHT A_SpiderdemonAttack;
            SPID G 3 BRIGHT A_EjectCasing;
            SPID H 1 BRIGHT A_SpiderdemonAttack;
            SPID H 3 BRIGHT A_EjectCasing;
            SPID H 1 BRIGHT A_SpidRefire;
            Goto Missile+1;
    }

    //===========================================================================
    //
    // RR_SpiderMastermind::BeginPlay
    //
    // Retrieves the casing type and the attack sound
    // from the default instance of RRWM's Shotgun.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();

        let shotgun = GetDefaultByType('RR_Shotgun');

        // Set up the casing type and the puff type.
        _casingType = shotgun.GetCasingType();
        _puffType = shotgun.GetPuffType();

        // Copy the attack sound.
        AttackSound = shotgun.AttackSound;
    }

    //===========================================================================
    //
    // RR_SpiderMastermind::A_SpiderdemonAttack
    //
    // Plays the attack sound and performs the Spider Mastermind's attack.
    //
    //===========================================================================
    void A_SpiderdemonAttack()
    {
        // The decal generator may have been reset upon loading a save,
        // so we should check every time if it's null and set it again.
        if (!DecalGenerator)
        {
            DecalGenerator = GetDefaultByType('RR_Shotgun').DecalGenerator;
        }

        int dmg = random[RR_SpiderMastermind__A_SpiderdemonAttack](1, 5) * 3;
        A_CustomBulletAttack(22.5, 0, 3, dmg, _puffType, 0, CBAF_NORANDOM | CBAF_NORANDOMPUFFZ);
    }

    //===========================================================================
    //
    // RR_SpiderMastermind::A_EjectCasing
    //
    // Ejects a shotgun casing.
    //
    //===========================================================================
    void A_EjectCasing()
    {
        if (RR_Settings.GetValue_Play(RR_SETTING_SPIDERDEMON_CASINGS))
        {
            // Note that we pass a radius factor of 0.5
            // because the Spider Mastermind is really big.
            // If we don't do this, it will look as if casings
            // are being spawned out of thin air.
            RR_DebrisBase.SpawnFromWeapon(
                _casingType,
                self, 0.5, 32,
                (-6, 0), (-0.5, 2, 0), (0.5, 1.0, 0)
            );
        }
    }
}
