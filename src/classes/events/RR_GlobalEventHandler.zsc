//===========================================================================
//
// RR_GlobalEventHandler
//
// Initializes and stores the global instances of the settings container.
// Handles re-initialization of persistent settings for a new game.
//
//===========================================================================
class RR_GlobalEventHandler : StaticEventHandler
{
    // All metadata on the game settings is stored here.
    // This way, they can be queried even when not in a level.
    private RR_Settings _settings;

    //===========================================================================
    //
    // RR_GlobalEventHandler::OnRegister
    //
    // Initialzies RRWM settings and prints RRWM version to the console.
    //
    //===========================================================================
    override void OnRegister()
    {
        _settings = RR_Settings.Create();

        // Perhaps not the best place to do it, but I'm not sure if creating
        // another event handler just to print this message is a good idea.
        Console.Printf(
            String.Format(StringTable.Localize("$RR_STARTUP_MESSAGE"),
            RR_VersionInfo.VERSION_STRING,
            RR_VersionInfo.VERSION_TIMESTAMP
        ));
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::NewGame
    //
    // Re-initializes all settings for a new game.
    //
    //===========================================================================
    override void NewGame()
    {
        _settings.GetGlobalPersistentSettings().SyncWithActualValues();
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::WorldLoaded
    //
    // Clears the settings cache.
    //
    //===========================================================================
    override void WorldLoaded(WorldEvent e)
    {
        // It appears that calling this in NewGame is not enough,
        // we have to call it each time a level loads.
        // Fortunately, only user CVARs are affected by this.
        _settings.ClearCache();

        // Trigger the version check
        let localEventHandler = RR_LocalEventHandler(EventHandler.Find('RR_LocalEventHandler'));
        if (!localEventHandler)
        {
            ThrowAbortException("%s: Couldn't find the local event handler to perform the version check.", GetClassName());
        }
        else
        {
            localEventHandler.CheckVersion();
        }
    }

    //===========================================================================
    //
    // RR_GlobalEventHandler::GetSettings
    //
    // Returns the global instance of RR_Settings.
    // This method is used by external sources to access settings.
    //
    //===========================================================================
    RR_Settings GetSettings() const
    {
        return _settings;
    }
}
