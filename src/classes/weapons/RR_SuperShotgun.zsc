//===========================================================================
//
// RR_SuperShotgun
//
// The double-barreled shotgun, aka the Super Shotgun.
//
//===========================================================================
class RR_SuperShotgun : RR_HitscanWeapon replaces SuperShotgun
{
    // Should the shell feed animation be played after reloading?
    private bool _shouldFeed;
    // When did we shoot the weapon?
    private int _shootTime;

    Default
    {
        Scale 0.5;
        Obituary "$OB_RR_SUPERSHOTGUN";
        Tag "$TAG_RR_SUPERSHOTGUN";
        AttackSound "rrwm/weapons/supershotgun/fire";

        Inventory.Icon "SOGPA0";
        Inventory.PickupSound "rrwm/weapons/supershotgun/pickup";
        Inventory.PickupMessage "$PICKUP_RR_SUPERSHOTGUN";

        Weapon.AmmoType 'RR_Shell';
        Weapon.AmmoUse 2;
        Weapon.AmmoGive 8;
        Weapon.SelectionOrder 400;
        Weapon.SlotNumber 3;

        RR_Weapon.SelectOffset 140, 5;

        RR_ReloadableWeapon.ClipCapacity 8;

        RR_HitscanWeapon.PuffType 'RR_ShotgunPuff';
        RR_HitscanWeapon.CasingType 'RR_ShotgunCasing';
        RR_HitscanWeapon.CasingSpawnOffset 6.0, -7.5;
        RR_HitscanWeapon.CasingSpawnVelocity 0.25, -2.0, 1.5;
        RR_HitscanWeapon.CasingVelocitySpread 0.1, 0.2, 0.2;
    }

    States(Actor)
    {
        Spawn:
            SOGP A -1;
            Stop;
    }

    States(Weapon)
    {
        // Select animation states
        SelectAnim:
            SOGX B 1;
            TNT1 A 0 A_PlayWeaponAuxSound("rrwm/weapons/supershotgun/select");
            SOGX B 1 A_MoveWeaponSprite(-33, -4);
            SOGX B 1 A_MoveWeaponSprite(-30, -3);
            SOGX B 1 A_MoveWeaponSprite(-27, -2);
            SOGX B 1 A_MoveWeaponSprite(-15, -1);
            SOGX B 1 A_MoveWeaponSprite(-13, 1);
            SOGX B 1 A_MoveWeaponSprite(-12, 2);
            SOGX B 1 A_MoveWeaponSprite(-10, 2);
            SOGX A 1 A_SuperShotgunCheckSelect;
            Goto Select;
        SelectAnimEnd_0_2:
            SOX2 A 1;
            Goto Select;
        // Ready states
        Ready_0_2:
            SOG2 A 1 A_ReloadableWeaponReady;
            Loop;
        Ready_4:
            SOG4 A 1 A_ReloadableWeaponReady;
            Loop;
        Ready_6_8:
            SOGG A 1 A_ReloadableWeaponReady;
            Loop;
        // Deselect states
        DeselectAnim_0_2:
            SOX2 A 1;
            Goto DeselectAnim_4_6_8 + 1;
        DeselectAnim_4_6_8:
            SOGX A 1;
            SOGX B 1;
            SOGX B 1 A_MoveWeaponSprite(10, -2);
            SOGX B 1 A_MoveWeaponSprite(12, -2);
            SOGX B 1 A_MoveWeaponSprite(13, -1);
            SOGX B 1 A_MoveWeaponSprite(15, 1);
            SOGX B 1 A_MoveWeaponSprite(27, 2);
            SOGX B 1 A_MoveWeaponSprite(30, 3);
            SOGX B 1 A_MoveWeaponSprite(33, 4);
            Goto Deselect;
        Fire_2:
            SOGF E 3 BRIGHT A_SuperShotgunAttack;
            SOGF F 3 BRIGHT;
            SOGG C 3;
            SOG2 DB 3;
            SOG2 A 7;
            TNT1 A 0 A_ReFire;
            Goto Ready_0_2;
        Fire_4:
            SOGF C 3 BRIGHT A_SuperShotgunAttack;
            SOGF D 3 BRIGHT;
            SOGG CD 3;
            SOG4 BA 3;
            Goto Feed_2;
        Fire_6_8:
            SOGF A 3 BRIGHT A_SuperShotgunAttack;
            SOGF B 3 BRIGHT;
            SOGG CDBA 3;
            Goto Feed_4;
        Flash:
            TNT1 A 3 A_Light1;
            TNT1 A 3 A_Light2;
            TNT1 A 0 A_Light0;
            Stop;
        Feed_2:
            SOG4 E 7;
            SOG4 F 3 A_SuperShotgunFeed;
            SOG4 G 3;
            SOG3 E 10 A_EjectCasing;
            SOG3 F 3 A_SuperShotgunFeed;
            SOG3 G 3;
            SOG2 E 10 A_EjectCasing;
            TNT1 A 0 A_SuperShotgunReFire;
            Goto Ready_0_2;
        Feed_4:
            SOGG E 7 A_SuperShotgunCheckFeed(false);
            SOGG F 3 A_SuperShotgunFeed;
            SOGG G 3;
            SOGG E 10 A_EjectCasing;
            SOG6 F 3 A_SuperShotgunFeed;
            SOG6 G 3;
            SOG4 E 10 A_EjectCasing;
            TNT1 A 0 A_SuperShotgunReFire;
            Goto Ready_4;
        Feed_6_8:
            SOGG E 7;
            SOGG F 3 A_SuperShotgunFeed;
            SOGG G 3;
            SOGG E 10 A_EjectCasing;
            SOGG F 3 A_SuperShotgunFeed;
            SOGG G 3;
            SOGG E 10 A_EjectCasing;
            TNT1 A 0 A_SuperShotgunReFire;
            Goto Ready_6_8;
        DryFire:
            SOG2 A 16 A_PlayWeaponAuxSound("rrwm/weapons/supershotgun/dryfire");
            TNT1 A 0 A_DryReFire;
            Goto Ready_0_2;
        Unload_0_2:
            SOG2 EHIJKLMN 2;
            Goto Reload;
        Unload_4:
            SOG4 EH 2;
            SOG4 I 2 A_SuperShotgunUnload;
            SOGG JKLMN 2;
            Goto Reload;
        Unload_6_8:
            SOGG EH 2;
            SOGG I 2 A_SuperShotgunUnload;
            SOGG JKLMN 2;
            Goto Reload;
        Reload:
            SOGR A 13 A_EjectClip;
            SOGR BCDEFGH 2;
            SOGR I 2 A_PlayWeaponAuxSound("rrwm/weapons/supershotgun/reload2");
            SOGR J 2 A_ReloadFullClip;
            SOGR KL 2;
            SOR4 M 1 A_SuperShotgunCheckLoad;
            Goto Load_2;
        Load_2:
            SOR4 NOPQR 1;
            SOG4 H 1;
            SOG4 E 2 A_SuperShotgunCheckFeed(true);
            SOG4 A 5;
            Goto Ready_4;
        Load_4_6_8:
            SOGR MNOPQR 1;
            SOGG H 1;
            SOGG E 2 A_SuperShotgunCheckFeed(true);
            SOGG A 5;
            Goto Ready_6_8;
    }

    //===========================================================================
    //
    // RR_SuperShotgun::A_SuperShotgunCheckSelect
    //
    // Jumps to SelectAnimEnd_0_2 if the clip amount is 0 or 2.
    //
    //===========================================================================
    action(Weapon) state A_SuperShotgunCheckSelect()
    {
        let clip = invoker.GetClipAmount();

        if (clip == 0 || clip == 2)
        {
            return ResolveState('SelectAnimEnd_0_2');
        }

        return null;
    }

    //===========================================================================
    //
    // RR_SuperShotgun::A_SuperShotgunAttack
    //
    // Fires the Super Shotgun.
    //
    //===========================================================================
    action(Weapon) void A_SuperShotgunAttack()
    {
        A_GunFlash();
        A_FireHitscan(11.25, 7.097, 20, 5);

        // Remember when we shot the weapon
        // to apply correct smoke supply to the casing later.
        invoker._shootTime = Level.maptime;
    }

    //===========================================================================
    //
    // A_SuperShotgunReFire
    //
    // Calls A_ReFire if not invoked from the reloading phase.
    //
    //===========================================================================
    action(Weapon) void A_SuperShotgunReFire()
    {
        if (invoker._shouldFeed)
        {
            invoker._shouldFeed = false;
        }
        else
        {
            A_ReFire();
        }
    }

    //===========================================================================
    //
    // RR_SuperShotgun::A_SuperShotgunFeed
    //
    // Plays the shell feed sound.
    //
    //===========================================================================
    action(Weapon) void A_SuperShotgunFeed()
    {
        A_PlayWeaponAuxSound("rrwm/weapons/supershotgun/feed");
    }

    //===========================================================================
    //
    // RR_SuperShotgun::A_SuperShotgunUnload
    //
    // Plays the unloading sound.
    //
    //===========================================================================
    action(Weapon) void A_SuperShotgunUnload()
    {
        A_PlayWeaponAuxSound("rrwm/weapons/supershotgun/reload1");
    }

    //===========================================================================
    //
    // RR_SuperShotgun::A_SuperShotgunCheckLoad
    //
    // Jumps to Load_4_6_8 if clip amount is 4 or more in case of a full reload,
    // or if clip amount is 6 or more in case of a partial reload.
    //
    //===========================================================================
    action(Weapon) state A_SuperShotgunCheckLoad()
    {
        let clip = invoker.GetClipAmount();

        // In case of a full reload, do the jump wih 4 or more rounds.
        bool shouldJump = clip == 4 || clip == 6 || clip == 8;

        // However, in case of a partial reload, only jump with 6 or 8 rounds.
        if (!invoker._shouldFeed)
        {
            shouldJump  = shouldJump && clip != 4;
        }

        if (shouldJump)
        {
            return ResolveState('Load_4_6_8');
        }

        return null;
    }

    //===========================================================================
    //
    // RR_SuperShotgun::A_SuperShotgunCheckFeed
    //
    // If invoked during the reloading phase, jumps to one of the Feed states
    // if the feed animation is set to be played.
    //
    // Otherwise, jumps to Feed_6_8 if the clip amount if 6.
    //
    //===========================================================================
    action(Weapon) state A_SuperShotgunCheckFeed(bool fromReload)
    {
        let clip = invoker.GetClipAmount();

        if (fromReload && invoker._shouldFeed)
        {
            switch (clip)
            {
                case 2:
                    return ResolveState('Feed_2');
                case 4:
                    return ResolveState('Feed_4');
                case 6:
                case 8:
                    return ResolveState('Feed_6_8');
            }

            return invoker.RaiseClipAmountError(clip);
        }

        if (!fromReload && !invoker._shouldFeed && clip == 6)
        {
            return ResolveState('Feed_6_8');
        }

        return null;
    }

    //===========================================================================
    //
    // RR_SuperShotgun::GetReadyState
    //
    // Returns the correct ready state based on the clip amount.
    //
    //===========================================================================
    override state GetReadyState()
    {
        let clip = GetClipAmount();

        switch (clip)
        {
            case 0:
            case 2:
                return ResolveState('Ready_0_2');
            case 4:
                return ResolveState('Ready_4');
            case 6:
            case 8:
                return ResolveState('Ready_6_8');
        }

        return RaiseClipAmountError(clip);
    }

    //===========================================================================
    //
    // RR_SuperShotgun::GetDeselectAnimState
    //
    // Returns the correct deselect animation state based on the clip amount.
    //
    //===========================================================================
    override state GetDeselectAnimState()
    {
        let clip = GetClipAmount();

        switch (clip)
        {
            case 0:
            case 2:
                return ResolveState('DeselectAnim_0_2');
            case 4:
            case 6:
            case 8:
                return ResolveState('DeselectAnim_4_6_8');
        }

        return RaiseClipAmountError(clip);
    }

    //===========================================================================
    //
    // RR_SuperShotgun::GetFireState
    //
    // Returns the correct fire state based on the clip amount.
    //
    //===========================================================================
    override state GetFireState(bool altFire, EFireType type)
    {
        if (altFire)
        {
            return Super.GetFireState(altFire, type);
        }

        let clip = GetClipAmount();

        switch (clip)
        {
            case 2:
                return ResolveState('Fire_2');
            case 4:
                return ResolveState('Fire_4');
            case 6:
            case 8:
                return ResolveState('Fire_6_8');
        }

        return RaiseClipAmountError(clip);
    }

    //===========================================================================
    //
    // RR_SuperShotgun::GetReloadState
    //
    // Returns the correct reload state (or, in this case, UNload state)
    // based on the clip amount.
    //
    //===========================================================================
    override state GetReloadState()
    {
        let clip = GetClipAmount();
        // Display shell feeding animation if the clip is currently empty.
        _shouldFeed = clip == 0;

        switch (clip)
        {
            case 0:
            case 2:
                return ResolveState('Unload_0_2');
            case 4:
                return ResolveState('Unload_4');
            case 6:
            case 8:
                return ResolveState('Unload_6_8');
        }

        return RaiseClipAmountError(clip);
    }

    //===========================================================================
    //
    // RR_SuperShotgun::Travelled
    //
    // Resets the shoot time when transitioning to another level.
    //
    //===========================================================================
    override void Travelled()
    {
        Super.Travelled();

        // We presume we last shot the weapon at an indefinite time in the past.
        // Granted, this will only have effect if the weapon was empty
        // before we travelled, but still.
        _shootTime = -1;
    }

    //===========================================================================
    //
    // RR_SuperShotgun::SpawnCasing
    //
    // Reduces the spawned casing's smoke supply
    // based on how much time has passed.
    //
    // The effect will be noticeable when reloading.
    // When firing normally, the casings will have their default smoke supply.
    //
    //===========================================================================
    override RR_CasingBase SpawnCasing(Actor source)
    {
        let casing = Super.SpawnCasing(source);
        // do not do any of this stuff if we don't have an owner.
        // (e.g. if a casing is being spawned by a monster via the weapon's GetDefaultByType instance)
        if (casing && Owner)
        {
            // If we don't know when we shot the weapon
            // (happens if we travelled to another level),
            // then we presume there is no smoke left (hence 0).
            //
            // Otherwise, we account for the delay between firing
            // and ejecting the casing. The casing will have
            // normal amount of smoke if ejected during normal fire.
            //
            // But if ejected after a full reload, it will have less smoke.
            // How much less depends on the delay.
            //
            // We divide the subtracted value by 2
            // because smoke doesn't dissipate that quickly
            // while the casing is still inside.
            casing.SmokeSupply = _shootTime == -1
                ? 0
                : Max(casing.SmokeSupply - (Level.maptime - _shootTime - 31) / 2, 0);
        }

        return casing;
    }

    //===========================================================================
    //
    // RR_SuperShotgun::RaiseClipAmountError
    //
    // Aborts VM execution with a special message,
    // signifying that the clip amount was somehow not correct.
    //
    //===========================================================================
    private state RaiseClipAmountError(int clip)
    {
        ThrowAbortException("%s: Invalid clip amount encountered: %d", GetClassName(), clip);
        return null;
    }
}
