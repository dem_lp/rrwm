//===========================================================================
//
// RR_PlasmaBall
//
// A bolt of pure energy fired by the Hyperblaster.
//
//===========================================================================
class RR_PlasmaBall : RR_Projectile
{
    // Determines how far behind the ball to spawn particles.
    // A steady stream of plasma balls fired from the same spot
    // will create a (more or less) uniformly distributed particle trail.
    private double _particleShiftFactor;

    Default
    {
        Radius 13;
        Height 8;
        Speed 25;
        Damage 5;
        Projectile;
        SeeSound "rrwm/projectiles/plasmaball/fire";
        DeathSound "rrwm/projectiles/plasmaball/explode";

        Obituary "$OB_RR_PLASMABALL";

        Decal "RR_PlasmaBallFade";
        RenderStyle "Add";
        Alpha 0.75;
        Scale 0.5;
        +FORCEXYBILLBOARD;
    }

    States
    {
        Spawn:
            HBLS AABB 2 BRIGHT Light("RR_PLASMABALL") A_SpawnPlasmaTrail;
            Loop;
        Death:
            HBLS CD 3  BRIGHT Light("RR_PLASMABALL_EXPLODE_1");
            HBLS EFG 3 BRIGHT Light("RR_PLASMABALL_EXPLODE_2");
            Stop;
    }

    //===========================================================================
    //
    // RR_PlasmaBall::A_SpawnPlasmaTrail
    //
    // Spawns a trail of energy particles behind the ball.
    //
    //===========================================================================
    void A_SpawnPlasmaTrail()
    {
        if (GetAge() < tics)
        {
            return;
        }

        let pOfs = RR_Math.GetRotatedOffset(ShiftOffsetByVel((
            -vel.length() * _particleShiftFactor * tics,
            frandom[RR_PlasmaBall__A_SpawnPlasmaTrail](-1, 1),
            frandom[RR_PlasmaBall__A_SpawnPlasmaTrail](-1, 1) + 2.5
        )), self);

        let pVel = RR_Math.GetRotatedOffset((
            frandom[RR_PlasmaBall__A_SpawnPlasmaTrail](0, -1),
            randompick[RR_PlasmaBall__A_SpawnPlasmaTrail](-1, 1),
            frandom[RR_PlasmaBall__A_SpawnPlasmaTrail](-1, 1)
        ), self) / 5;

        A_SpawnParticle("grey", SPF_FULLBRIGHT, 50, 3, 0, pOfs.x, pOfs.y, pOfs.z, pVel.x, pVel.y, pVel.z);
    }

    //===========================================================================
    //
    // RR_PlasmaBall::BeginPlay
    //
    // Initializes the particle shift factor.
    //
    //===========================================================================
    override void BeginPlay()
    {
        Super.BeginPlay();
        _particleShiftFactor = frandom[RR_PlasmaBall__BeginPlay](0, 1);
    }
}
