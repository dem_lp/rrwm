##############################################################################
#                                                                            #
# RRWM PowerShell build script                                               #
# by Player701                                                               #
#                                                                            #
# This script has two modes: a build-only mode and a build+test mode.        #
# The mode is specified by the "-mode" argument. Default is "build",         #
# for build+test use "test".                                                 #
#                                                                            #
# Other arguments:                                                           #
#                                                                            #
# -dir_src     <path> : Override the path to RRWM sources.                   #
#                       Default is "src".                                    #
#                                                                            #
# -dir_tools   <path> : Override the path to building tools.                 #
#                       Default is "tools".                                  #
#                       Currently, only 7-Zip is used for building.          #
#                       Pandoc is used to convert Markdown documents         #
#                       to PDFs when preparing a release archive.            #
#                       Using Pandoc is optional; if it is not found,        #
#                       PDF creation will be skipped.                        #
#                                                                            #
# -dir_build   <path> : Override the output path where the resulting         #
#                       PK3 file will appear. Default is "build".            #
#                                                                            #
# -test_file   <path> : Path to a text file which contains the GZDoom path   #
#                       on the first line and GZDoom arguments on the second #
#                       line. Adding an argument to load RRWM itself is      #
#                       unnecessary. Defaults to "build.test", will be       #
#                       generated if it doesn't exist and updated            #
#                       automatically.                                       #
#                                                                            #
# -gzdoom_path <path> : Path to GZDoom. If omitted, the path from the test   #
#                       file will be used.                                   #
#                                                                            #
# -gzdoom_args <args> : Command-line arguments to pass to GZDoom. Adding     #
#                       an argument to load  RRWM itself is unnecessary.     #
#                       Defaults to an empty string. If omitted,             #
#                       the arguments from the test file will be used.       #
#                                                                            #
##############################################################################
param(
    [string] $mode = "build",
    [string] $dir_src,
    [string] $dir_tools,
    [string] $dir_build,
    [string] $test_file = "build.test",
    [string] $gzdoom_path,
    [string] $gzdoom_args
);

$project_name = "rrwm";
$project_ext  = "pk3";
$version_template_name = "VersionInfoTemplate.txt";

# We have to terminate on uncaught errors.
$ErrorActionPreference = "Stop";

# Get the path to this script (PowerShell v1 compatible).
$script_root = Split-Path -Parent $MyInvocation.MyCommand.Definition;

# Main()
# Entry point for the script.
function Main()
{
    Write-Section "RRWM build script by Player701";

    # Get the display string for the current mode
    # and also check if the mode is valid.
    $displayMode = Check-Mode($mode);

    Write-Host "Mode: $displayMode";

    # Resolve full paths for all directories.
    $dir_src   = Get-Path $dir_src   "src";
    $dir_tools = Get-Path $dir_tools "tools";
    $dir_build = Get-Path $dir_build "build";

    Write-Host "Source path: $dir_src";
    Write-Host "Tools path: $dir_tools";
    Write-Host "Output path: $dir_build";

    # Do stuff based on the provided mode.
    switch ($mode)
    {
        "build"   { Do-Build; }
        "test"    { Do-Build; Do-Test; }
        "release" { Do-Release $(Do-Build); }
    }

    Write-Section "Finished";
    return 0;
}

# Get-Path()
# Returns the resolved absolute path from the provided value,
# or a default path if the value is empty.
function Get-Path($path, $default_path)
{
    # If path is not defined,
    # use a path relative to the script root directory.
    if (-not $path)
    {
        return Join-Path $script_root $default_path;
    }

    # Resolve the path normally if it is defined.
    return Resolve-Path $path;
}

# Check-Mode()
# Used to verify the mode and return a friendly name for it.
function Check-Mode($mode)
{
    switch ($mode)
    {
        "build"   { return "Build only"; }
        "test"    { return "Build and test"; }
        "release" { return "Build and make a release archive"; }

        default { Throw "Invalid mode: `"$mode`""; }
    };
}

# Get-Project-File-Name()
# Returns the project name with an absolute path.
function Get-Project-File-Name()
{
    return Join-Path $dir_build "$project_name.$project_ext";
}

# Do-Build()
# Performs build tasks.
function Do-Build
{
    $version_string = Update-Version;
    Package-Files;

    return $version_string;
}

# Update-Version()
# Updates RRWM version info from Git.
function Update-Version()
{
    Write-Section "Updating version info";

    $version_template_file = Join-Path $dir_src $version_template_name;
    $version_output_file = Join-Path $dir_src "classes\common\RR_VersionInfo.zsc";

    # Make sure the version template file exists.
    if (-not (Test-Path $version_template_file))
    {
        Throw "Error: could not find template file $version_template_file";
    }

    # Get the version string from Git.
    $version_string = & "git" "describe" "--tags" "--dirty=-m";
    if (-not $?)
    {
        Write-Warning "Error getting version string: Git returned exit code $LastExitCode";
        $version_string = "Unknown";
    }
    else
    {
        # Get the version timestamp from Git.
        $version_timestamp = $version_string | & "git" "log" "-1" "--format=%ai";
        if (-not $?)
        {
            Write-Warning "Error getting version timestamp: Git returned exit code $LastExitCode";
        }
    }

    # If failed to get the timestamp, fallback to "Unknown".
    if (-not $version_timestamp)
    {
        $version_timestamp = "Unknown";
    }

    Write-Host "Version string: $version_string";
    Write-Host "Version timestamp: $version_timestamp";

    # This scary-looking thing reads the template file's contents,
    # substitutes placeholders with actual values and writes
    # the destination file with the substituted values.
    (Get-Content $version_template_file)                                  `
    | Where { $_ -notmatch "^\/\/!" }                                     `
    | % { $_.replace("%VERSION_STRING%",        $version_string       ) } `
    | % { $_.replace("%VERSION_TIMESTAMP%",     $version_timestamp    ) } `
    | % { $_.replace("%VERSION_TEMPLATE_NAME%", $version_template_name) } `
    | Set-Content $version_output_file;

    Write-Host "Values written to: $version_output_file";
    return $version_string;
}

# Package-Files()
# Creates a PK3 archive with RRWM source files.
function Package-Files()
{
    Write-Section "Packaging files";

    $archiver_path = Get-Archiver;
    $project_file = Get-Project-File-Name;

    Archive-Files $archiver_path $dir_src "zip" $project_file "-x!$version_template_name"
}

# Archive-Files()
# Creates an archive from the contents of a directory.
function Archive-Files($archiver_path, $input_dir, $archive_type, $output_file, $extra_args)
{
    $log_file = Replace-Extension $output_file "log";

    & $archiver_path "a" "-t$archive_type" "-mx=9" "-m0=lzma" "-bb2" $output_file "$input_dir\*" "-up0q0r2x2y2z1w2" $extra_args `
    | Tee-Object $log_file `
    | Log-With-Prefix      `
    | Write-Host;          `

    # Check that we've successfully packaged all files.
    if (-not $?)
    {
        Throw "Error packaging files: 7-Zip returned exit code $LastExitCode";
    }

    Write-Host "Files packaged successfully.";
    Write-Host "Output file: $output_file";
    Write-Host "Log file: $log_file";
}

# Replace-Extension()
# Replaces the extension of the file name with a new one.
function Replace-Extension($file, $new_ext)
{
    $dir = [System.IO.Path]::GetDirectoryName($file);
    $file_no_ext = [System.IO.Path]::GetFileNameWithoutExtension($file);
    return Join-Path $dir "$file_no_ext.$new_ext";
}

# Log-With-Prefix()
# Append ">> " to piped input to indicate it came from an external program.
function Log-With-Prefix
{
    param([parameter(ValueFromPipeline=$true)] $x);
    Process
    {
        return ">> $x";
    }
}

# Get-Archiver
# Gets the path to the 7-Zip archived.
function Get-Archiver()
{
    $archiver_path = Join-Path $dir_tools "7z\7z.exe";
    if (-not (Test-Path $archiver_path))
    {
        Throw "Could not find 7-Zip archiver at $archiver_path";
    }

    return $archiver_path;
}

# Do-Test()
# Performs tasks to run GZDoom for testing.
function Do-Test
{
    Write-Section "Preparing GZDoom for testing";
    $test_params = Get-Test-Parameters;
    $gzdoom_path = $test_params[0];
    $gzdoom_args = $test_params[1];
    Run-GZDoom;
}

# Run-GZDoom()
# Runs GZDoom after its path and arguments have been resolved.
function Run-GZDoom()
{
    # Check that the GZDoom executable exists.
    if (-not (Test-Path $gzdoom_path))
    {
        Throw "Error: GZDoom executable not found at the specified path.";
    }

    $project_file = Must-Get-Project-File-Name;

    # If arguments are provided, split them to individual parts.
    # Otherwise, they will not be recognized.
    if ($gzdoom_args)
    {
        $gzdoom_args = $gzdoom_args.Split(" ");
    }

    Write-Host "Starting GZDoom...";
    & $gzdoom_path "-file" $project_file $gzdoom_args;
}

# Must-Get-Project-File-Name()
# Returns the project file name
# or throws an error if the file doesn't exist.
function Must-Get-Project-File-Name()
{
    $result = Get-Project-File-Name;

    # Check that the project file exists.
    if (-not (Test-Path $result))
    {
        Throw "Error: project file not found in the output directory.";
    }

    return $result;
}

# Get-Test-Parameters()
# Gets GZDoom path and argument from the command line or the test file,
# based on where they are found. Command line has preference.
# Updates the test file afterwards.
function Get-Test-Parameters()
{
    # By default, we'll use the command line.
    $result = $gzdoom_path, $gzdoom_args;

    # Check that the test file exists.
    $test_file = Join-Path $pwd $test_file;
    $test_path = Test-Path $test_file;

    if ($test_path)
    {
        Write-Host "Test file: $test_file";
        $testfile_data = Get-Content $test_file -TotalCount 2;

        # If GZDoom path is not specified, try to use data from the test file.
        if (-not $gzdoom_path)
        {
            if ($testfile_data.length -gt 0)
            {
                $result[0] = $testfile_data[0];
                Write-Host "Using GZDoom path from test file.";
            }
            else
            {
                Throw "Error: GZDoom path not specified and not found in test file.";
            }
        }

        # If GZDoom arguments are not specified, try to use arguments from the test file.
        if ((-not $gzdoom_args) -And ($testfile_data.length -gt 1))
        {
            $result[1] = $testfile_data[1];
            Write-Host "Using GZDoom arguments from test file.";
        }
    }
    else
    {
        # If not test file found, check that GZDoom path is provided.
        Write-Host "No test file found.";

        if (-not $gzdoom_path)
        {
            Throw "Error: GZDoom path not specified.";
        }
    }

    # Output the resulting paths.
    Write-Host "GZDoom path: $($result[0])";
    if ($result[1])
    {
        Write-Host "GZDoom arguments: $($result[1])";
    }

    try
    {
        # Try to update the test file. It is not a critical error if we fail to do that,
        # but we still need to print a warning message.
        if (-not $test_path)
        {
            Write-Host "Saving parameters to test file: $test_file";
        }
        else
        {
            Write-Host "Updating test file: $test_file";
        }

        Set-Content -Path $test_file -Value $result;
    }
    catch
    {
        # We couldn't update the file, inform the user about it.
        Write-Warning "Couldn't write to test file: $_";
    }

    return $result;
}

# Do-Release()
# Creates a release archive in the build folder.
function Do-Release($version_string)
{
    Write-Section "Creating release archive";

    $project_file = Must-Get-Project-File-Name;
    $archiver_path = Get-Archiver;
    $release_path = Get-Release-Path;
    $staging_path = Get-Staging-Path $release_path;
    $release_file = Get-Release-Archive-Name $release_path $version_string;

    # Create the staging folder
    Create-Directory $staging_path;
    Try
    {
        # Create PDFs
        Make-Documents $staging_path;

        # Copy the package and archive it
        Copy-Item $project_file $staging_path;
        Archive-Files $archiver_path $staging_path "7z" $release_file
    }
    Finally
    {
        # Even if an error occurs, clean up the staging folder afterwards
        Remove-Item $staging_path -Force -Recurse;
    }
}

# Get-Release-Path()
# Returns the path to generate release archives at.
function Get-Release-Path()
{
    return Join-Path $dir_build "release";
}

# Get-Release-Archive-Name()
# Returns the name of the release archive to generate.
function Get-Release-Archive-Name($release_path, $version_string)
{
    return Join-Path $release_path "$project_name-v$version_string.7z";
}

# Get-Staging-Path()
# Returns the staging path.
function Get-Staging-Path($release_path)
{
    return Join-Path $release_path "staging";
}

# Get-Pandoc()
# Returns the path to Pandoc.
function Get-Pandoc()
{
    return Join-Path $dir_tools "pandoc\pandoc.exe";
}

# Create-Directory()
# Creates a directory, deleting it first if it already exists.
function Create-Directory($path)
{
    if (Test-Path $path)
    {
        Remove-Item $path -Force -Recurse;
    }

    New-Item -Path $path -ItemType "directory" | Out-Null;
}

# Make-Documents()
# Creates PDFs from Markdown document files.
function Make-Documents($staging_path)
{
    $pandoc_path = Get-Pandoc;
    if (-not (Test-Path $pandoc_path))
    {
        Write-Warning "Could not find Pandoc at $pandoc_path";
        Write-Warning "PDF generation will be disabled."
    }
    else
    {
        Write-Host "Creating PDFs...";
        Make-Pdf $pandoc_path "README" $staging_path;
        Make-Pdf $pandoc_path "CREDITS" $staging_path;
    }
}

# Make-Pdf()
# Creates a PDF file from a Markdown file from the root folder.
function Make-Pdf($pandoc_path, $input_file, $staging_path)
{
    $orig_file = Join-Path $script_root "$input_file.md";
    if (-not (Test-Path $orig_file))
    {
        Throw: "Could not find document file $orig_file";
    }

    $pandoc_args = "-V geometry:margin=1in --variable urlcolor=blue";
    $output_file = Join-Path $staging_path "$input_file.pdf".ToLower();

    # Replace links to external resources within the repository (images etc.)
    # with their absolute paths so that Pandoc can see them.
    (Get-Content $orig_file)                                                             `
    | % { $_ -replace "\(\/(.*)\?raw=true\)", ("($script_root\`$1)" -replace "/", "\") } `
    | & $pandoc_path $pandoc_args.Split(" ") "-o" $output_file;

    # Check that the operation has succeeded.
    if (-not $?)
    {
        Throw "Error creating PDF: Pandoc returned exit code $LastExitCode";
    }

    Write-Host "Created $output_file";
}

# Write-Section()
# Writes a nicely formatted string indicating the start of the next build phase.
function Write-Section($what)
{
    Write-Fixed "";
    Write-Fixed $what;
    Write-Fixed "";
}

# Write-Fixed
# Writes a string enclosed in "-" characters, the result is always of fixed length.
function Write-Fixed($what)
{
    # Enclose non-empty string in spaces.
    if ($what -ne "")
    {
        $what = " " + $what + " ";
    }

    $rowLength = 80;
    $curLength = $what.length;

    # Only do this stuff if our string is shorter than the fixed width value,
    # otherwise it wouldn't fit anyway.
    if ($curLength -lt $rowLength)
    {
        function String-Join($char, $length)
        {
            return $("-" * $length);
        }

        $left = [Math]::Truncate(($rowLength - $curLength) / 2);
        $right = $rowLength - $curLength - $left;

        $fillChar = '-';
        $what = (String-Join $fillChar $left) + $what + (String-Join $fillChar $right);
    }

    Write-Host $what;
}

# Start the script here.
exit Main;
