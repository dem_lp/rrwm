# RRWM

## What is it?

RRWM is a complete ZScript rewrite of [a weapon mod with a similar name](https://www.doomworld.com/idgames/combos/rrwmr2) released on idgames in 2010. There have been several improvements to the released version in the mod's SVN repository (a link to it can be found in one of the readme files that come with the mod), but no further releases were ever made. Eventually, I concluded that improving the mod further without turning its code base into a complete mess would not be productive because of the inherent limitations of DECORATE, so I had to abandon it until better times. And now these better times have finally come. With the advent of ZScript, the limits have been transcended, and the impossible has become possible.

**Enter RRWM.**

In this new form, RRWM is first and foremost a coding experiment and only after that it is a weapon mod. The primary goal of this project is to leverage the power of ZScript to produce hack-free, well-readable, thoroughly commented code, which is less buggy than DECORATE and easier to maintain. Do not expect to see a lot of fancy special effects in this mod.

## The reloading system

The staple of the old RRWM was its reloadable weapons. After all, it wasn't named "Real *Reload* Weapon Mod" for nothing. And just like in old versions, today the reloading system still remains a core feature of this project. However, this time it has been completely redesigned with the power of scripting. Virtually nothing remains of the old system, which, nonetheless, was perhaps the best that could be achieved at that time without too many hacks. But compared to what can be done now, it became clear that the weapon reloading mechanic should be re-implemented entirely from scratch. The result is a robust and easy-to-use base class (RR_ReloadableWeapon), which can be inherited from to make a weapon reloadable with nothing more than two function calls and one new state sequence.

Here are the main benefits of the new system compared to the old one:

* Complete elimination of identical parts of code in different weapons. The old system had to be re-implemented in each new weapon. The new system defines all its behavior in the base class and provides a simple interface to access this behavior in child classes. Aside from making it easier to change the system (no need to go through every weapon and rewrite the code), it also greatly reduces the risk of accidentally introducing bugs with careless copy-pasting.
* The weapon clip and its capacity are now inherent properties of the weapon itself. This means no extra ammo types are needed to be defined, and we don't have to care about stuff like their backpack amounts, of if the player's inventory is tampered with by an outside force.
* The above means that the weapon uses only one ammo type instead of using Ammo1 for the clip and Ammo2 as the actual ammo. This enables the weapon to use Ammo2 for an alternate attack mode (such as a grenade launcher, for example). Although no weapons in RRWM use a secondary ammo type or have an alternate attack mode, it doesn't mean it is not possible. The code is there, it has been tested to work and is free to use by anyone (but please give credit for using it).
* The HUD can distinguish in a robust way between reloadable weapons and weapons that use two ammo types for different attack modes. By "robust way" I mean that we do not have to define a hard-coded list of weapons that should be treated by the HUD as reloadable, as the HUD code can make this distinction by itself by checking if the current weapon is a descendant of the reloadable weapon class.
* A huge drawback of the old system was that the weapons had the AMMO_OPTIONAL flag, which meant that they could be selected even when they had no ammo. Without this flag, it was very difficult, if not outright impossible -- at least with the relatively simple way the old system used -- to make reloading work properly without accidentally preventing the player from selecting the weapon. Weapons that use the new reloading system do not have this flag. This means that switching to an empty weapon is no longer allowed -- which is how it should really be.

## Gameplay

The secondary goal of RRWM, which is no less important than the primary one, is to avoid tampering with vanilla Doom gameplay experience too much while still managing to provide improvements to it. In other words: if you want a largely familiar, classic Doom experience with a tiny bit of enhancement brought into it, then RRWM is for you. All of RRWM's weapons serve as direct replacements for vanilla Doom weapons and function more or less the same. The only change that has a noticeable impact on the gameplay is, of course, the weapon reloading feature; perhaps it does promote a somewhat more cautious and less aggressive play style. Other than the reloading system, there are the following changes:

* The new Chainsaw behaves a tad different than the original. If there is, by chance, anyone here who remembers the old release (which is highly unlikely, but still), they will know what I mean. Otherwise, you have to try it out yourself. Of course, the weapon code has been re-implemented from scratch in ZScript and works even better than before, allowing for cool effects that couldn't have been possible with the DECORATE system.
* The Berserk replacement (called Adrenaline) now offers a 50% damage protection effect that lasts for 30 seconds and can be extended with additional Adrenaline pickups. In an homage to Quake II, the Adrenaline also increases the player's normal maximum health (reachable with medikits and stimpacks only) by one extra point, up to 200.

There are a few other small changes and improvements, but most of them can be adjusted or disabled in the options menu. Speaking of which, let's discuss...

## Customization and addons

Some of RRWM's new features can be customized. All customization is done through RRWM's special options menu, which is accessed from the very bottom of the main GZDoom options menu:

![Screenshot](/rrwm-options.png?raw=true)

Many of the settings available in this menu do not affect the gameplay and only provide visual enhancements. However, certain settings can have some impact on the gameplay, with some particular settings providing a more significant impact than others. For every setting, this and other extra information, including a full-text description, is available immediately from within the menu itself -- select an option and press F1 to be presented with a help page for it.

RRWM is also self-complete and thus doesn't provide any addon files. This is to make life easier for both the user and the mod developer. You do not have to load any extra files to tailor the gameplay experience to your liking. And I don't have to maintain complicated build scripts that compile all these files. Neither do I need to test if multiple addon files work fine when loaded alongside each other in various combinations.

Instead of separate addons, RRWM has so-called persistent settings. You adjust these settings before starting a new game, and they stay unchanged throughout it. The reason for why a setting is made persistent is not always the same. One is to provide consistent gameplay experience -- e.g. you've chosen the Railgun to replace the Rocket Launcher, and bringing the latter back in the next level could make you end up with both of them, breaking the concept of a permanent replacement. Other settings have been made persistent to avoid needlessly complicating the code, making it account for sudden changes of the setting ("Monsters reload weapons" is an example of this). Regardless of the justification, think of each of these settings as of a mini-addon -- only that you don't have to manually load it into GZDoom when you need it.

## HUD

RRWM replaces the standard GZDoom fullscreen HUD with its own. This is mainly to allow the currently selected weapon's clip amount to be displayed. The HUD also provides other useful information, such as the remaining time of all currently active powerups. Note that this is only supported for powerups that have a valid icon and vanilla Doom powerups (radiation suit, invulnerability sphere, blur sphere, light amp, berserk).

The HUD is also customizable. Quite a few settings in the RRWM options menu allow you to controls various aspects of the HUD. For example, you can turn the mugshot on or off, or choose which level statistics counters are displayed and when (you can make a counter hide itself automatically if its total value on the current level is 0, e.g. no monsters to kill or no secrets to find). The HUD is also multiplayer-ready, and will display a frag counter in deathmatch games.

The HUD works best if you enable the "Center messages" option, but nothing bad will happen if you have this option turned off. In fact, the only part of the HUD this option affects is the underwater air supply indicator, which does not appear at all in vanilla maps due to the lack of underwater areas. If messages aren't centered, the air supply indicator will be shifted downwards to make room for them.

If you don't like the new HUD, the original Doom status bar is still available. It has been modified to work with RRWM weapons and will display the current weapon's clip amount in the ammo section if the weapon is reloadable. This is probably not very convenient, so I don't actually recommend using it. The reason the status bar is there is because I didn't want to remove it entirely or leave it unchanged and thus broken, since the original status bar obviously doesn't know anything about RRWM weapons.

Using custom HUDs with RRWM is not recommended. Like the vanilla Doom status bar, these HUDs also don't know anything about RRWM's reloadable weapons, so they won't display clip amounts. Also, RRWM uses extra information provided by its custom powerup classes when drawing powerup icons. This special handling is implemented for the Adrenaline powerup so that the user can distinguish between its time-limited protection effect and its berserk effect, which may or may not be time-limited depending on the value of the corresponding option. Custom HUDs don't know how to account for this and will display both icons simultaneously, which is not intended behavior. This is also the reason why RRWM's powerup display is used even in status bar mode.

## Technical

### Requirements

To play RRWM, you need at least GZDoom 4.0.0. Earlier versions do not have the new features that RRWM makes use of. As an interesting fact, some of them have been implemented by me to avoid resorting to hacks in RRWM code. The most important one is probably the [event handler order fix](https://forum.zdoom.org/viewtopic.php?t=62398), which allows the settings system to function properly. Without it, GZDoom would crash outright when exiting the game, provided it could compile RRWM's scripts.

It is recommended to always use the latest version of GZDoom for the optimal experience. Get the latest GZDoom version on the [official download page](https://zdoom.org/downloads). See [the official wiki](https://zdoom.org/wiki/Installation_and_execution_of_ZDoom) for instructions on how to install GZDoom and play the mod.

### Compatibility with other mods and maps

RRWM is **not guaranteed** to be compatible with any other mods that replace existing weapons or items. This also applies to DEHACKED patches, _including those altering existing monsters_ -- in this particular case, RRWM should be loaded _before_ such a patch so as to avoid any potential consequences of [this sort](https://forum.zdoom.org/viewtopic.php?f=122&t=62948), although 100% glitch-free behavior is still not guaranteed even then. Bug reports for issues encountered while playing RRWM with such mods will not be investigated unless the issue in question can be reproduced in a setup where no mods other than RRWM itself are loaded. If you wish to play RRWM with such mods, you are completely on your own (and that also includes Brutal Doom). If you wish to make RRWM compatible with some other mod, feel free to do whatever you want with the code, but don't expect your changes to make it into the master branch. It is impossible for a mod to be compatible with everything, and RRWM is not aiming for that. Adding extra code to support such compatibility can lead to subtle bugs being introduced, and the more code there is, the higher the risk.

RRWM is also **not guaranteed** to be compatible with any other mods that replace existing enemies, but there is a chance it may work just fine. Depending on the order the mods are loaded in, some RRWM settings such as "Monsters reload weapons" or "Replace Cyberdemon's rockets" may not work due to the enemies these settings affect being replaced by another mod.

RRWM is **guaranteed** to be compatible with any vanilla map and any non-vanilla map that doesn't use any GZDoom features (e.g. a Boom map). If a map uses GZDoom features, there is a possibility that something may not work correctly. This applies mostly to ACS scripts manipulating the player's inventory. Unfortunately, the ACS functions reponsible for that do not take replacements into account, so if a level has a script that gives the player e.g. a shotgun, you will end up with the vanilla Doom shotgun and not the RRWM's version of it. This cannot be fixed without changing GZDoom's source code, but there is probably a reason why it works like that. Other scripts and GZDoom map features *should* be fine. Custom inventory items, weapons and enemies that do not replace any existing actors will probably not interfere with RRWM either.

**Bottom line:** if it looks like something is not working correctly and you are playing either a non-GZDoom map or a GZDoom map that you know doesn't use inventory-manipulating ACS, please report this supposedly incorrect behavior as a bug (please see the "Reporting bugs" section further below). However, there is no point in reporting if you are playing RRWM with another mod that replaces weapons or items. In this case, please check if the issue occurs without that mod, and report if it does. Your report will be investigated in due time. If you do not follow these guidelines, your report will also be looked into, but no promise is given that the issue you've reported can (or will) be fixed.

### Recommended map packs

Any PWAD with vanilla or Boom-compatible maps will generally play well with RRWM. However, "slaughtermaps" will become a lot more difficult since you can no longer keep shooting until you run out of ammo. The need to reload can be a serious tactical disadvantage when you don't have enough room to retreat; because of this, it is recommended to avoid levels that unleash numerous strong enemies on the player in confined spaces. Though if you still want to try, no one is stopping you -- but if you play without saves, you're in for a lot of frustration. Well, you practically asked for it, didn't you?

RRWM has been playtested in single player on the Ultra-Violence diffculty with the following IWADs and PWADs, which have been found to be completable without the use of cheat codes:

* Ultimate Doom
* Final Doom: TNT Evilution
* Final Doom: The Plutonia Experiment
* [Plutonia 2](https://www.doomworld.com/idgames/levels/doom2/megawads/pl2) (yes, even MAP32)
* [Alien Vendetta](https://www.doomworld.com/idgames/levels/doom2/megawads/av)
* [Armadosia](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/amdosia)
* [Hadephobia](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/h_phobia)
* [Plutonia: Revisited Community Project](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/prcp)
* [Unholy Realms](https://www.doomworld.com/idgames/levels/doom2/megawads/ur) (incl. MAP30, which starts to lag horribly towards the end)
* [Reverie](https://www.doomworld.com/idgames/levels/doom2/megawads/reverie)

Additionally, the following PWADs are recommended if you're looking for a map pack to try out RRWM with:

* Community Chest series ([first](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest), [second](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest2), [third](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest3), [fourth](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/cchest4))
* [Icarus: Alien Vanguard](https://www.doomworld.com/idgames/themes/TeamTNT/icarus/icarus)
* [Memento Mori](https://www.doomworld.com/idgames/themes/mm/mm_allup) and [Memento Mori II](https://www.doomworld.com/idgames/themes/mm/mm2)
* [Requiem](https://www.doomworld.com/idgames/levels/doom2/megawads/requiem)
* [Whispers of Satan](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/wos)
* [Zones of Fear](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/zof)
* [Newdoom Community Project](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/ndcp)
* [Japanese Community Project](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/jpcp)

### Multiplayer

RRWM has been playtested in cooperative multiplayer mode with the [Alien Vendetta PWAD](https://www.doomworld.com/idgames/levels/doom2/megawads/av). No network synchronization issues have been encountered, and it is thus believed that RRWM should work fine in any multiplayer mode, be it cooperative, deathmatch, or team game. If your network game de-syncs, you should first verify that all players are running the same version of GZDoom and loading the same files in the same order, and that the files themselves are also identical. Only if that's the case, then the issue can be considered a bug and should be reported (please see the "Reporting bugs" section below).

### Reporting bugs

When the new codebase for RRWM was first conceived, it wasn't possible to account for every single test case to verify that everything worked exactly as it was supposed to. Some of these cases were simply overlooked, while others were considered trivial and didn't get much attention initially. Certain assumptions made about the logic of some of GZDoom's built-in functionality weren't properly checked and later turned out to be wrong. As a result, parts of RRWM's code had to be changed to account for previously neglected test cases and fix the broken logic. Some classes had to be rewritten entirely, because the way they were designed turned out to be fundamentally wrong, and all attempts to fix them were creating problems somewhere else.

Since November 2018, RRWM's development cycle has gone through several RC stages and accumulated countless hours of continuous playtesting, both in singleplayer and multiplayer modes. However, even despite all these efforts, it is still possible that you may encounter a bug. If you think something in RRWM is not working as it should, then it's probably a bug and you're encouraged to report it so that it can be fixed.

When reporting bugs, please follow the usual GZDoom bug reporting guidelines. Note that it is not necessary to construct a test map if you don't feel like it (or don't know how to do that) or if it isn't needed to demonstrate the issue. However, please describe the testing procedure step-by-step -- what exactly should I do to experience buggy behavior? Screenshots and videos are optional and not a substitute for a proper testing procedure. If you are playing with some other mod (mind the compatibility notes above!), please check whether the issue appears only when you include that mod, or if the issue can be reproduced without the mod. Please provide links to all third-party content you are using, including the map pack (if any).

## Sprite contributions

RRWM suffers from the lack of its own original sprites. The current sprites were initially used as placeholders, but I've grown used to them over time. Yet, it would be much better if there were some original artwork in the mod.

Unfortunately, I'm not an artist and can only do very simple picture editing at best (e.g. add an alpha channel, crop an image, perform per-pixel edits), so anyone who wants to contribute sprites to this project will be welcomed with open arms. However, please note that not every sprite set can be included in RRWM. Due to the specific goals pursued by this project, the following requirements must be met for weapon sprites:

* They must have widescreen support. There are some entries on Realm667 I could've used in RRWM if not for this problem -- the sprites look incomplete in widescreen resolutions because the sprite author apparently thought that no one plays Doom in widescreen.
* They must resemble the original weapon at least on the very basic level. For example, a sniper rifle will not work as a replacement for any of the weapons, because there is simply no such thing as a sniper rifle in Doom. On the other hand, a bulky, futuristic-looking energy weapon could be a fitting replacement for the BFG. The only exception is the Railgun, which is an RRWM addition, but then again, a sniper rifle would probably not pass for a railgun either. (Also, there is no requirement for a Railgun replacement to have a scope on it.)
* They must be drawn in "Doom stlye". At the very least, this implies that standard Doomguy hands must be present (if there are any frames with hands, of course). The style of the weapon itself matters too, as it must be kept consistent with that of other weapons. The current BFG replacement is a good example of how far the style can deviate from the average.
* There has to be a distinguishable reloading animation. Sorry, I wish I could draw one myself, but, like I said, I'm not an artist... Also, the animation should not be limited to simply moving the frame down and right (or left), indicating that the actual manipulations with the weapon are happening off-screen. It doesn't look convincing in nearly every case. Again, really sorry for that, but I have tried such things before and they all look terrible -- the current sprite set for the Railgun is a perfect example. The Chainsaw and the Fist are, of course, exempt from this requirement.
* In addition to the above. Examples of a good reloading animation in the current version of RRWM: Rocket Launcher, Plasma Rifle, Super Shotgun, Shotgun, Chaingun. The Pistol is only slightly worse than all of those since we don't actually see the clip being ejected, but it's still fairly decent and convincing. The BFG is the worst example in the current version, but even then, there is some real animation there and not just moving frames. (No offense here, just personal opinion, and I'm in no way saying that the sprites or the animations are bad, quite the contrary!)
* It is preferable if the contributed sprite set also has sprites for ammo pickups. This applies mainly to Pistol, Chaingun, Rocket Launcher and Railgun replacements (cell ammo has a very fitting pickup sprite right now IMO, but if you have a better option to suggest, go ahead!). Same for empty clip sprites -- they are specific for each weapon and it's difficult to re-use them with other sprite sets.

If anyone has a sprite set they wish to contribute to RRWM, there is a fairly high chance that it will eventually make it to the next stable release, provided it meets all of the above requirements. The author of the sprites will be properly credited in the RRWM credits list file bundled with the next release.
