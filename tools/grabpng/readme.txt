  Usage: grabpng [[options] <file1> [file2] [...]]

  Installation:
  
    Put grabpng anywhere you like. Make a shortcut to it on your
    desktop or in your programs menu. If you want to use grabpng
    in batch mode, you'll want to add its location to your PATH
    environment variable.
  
    If you are not sure what your PATH environment variable is, 
    but you still want to use grabpng in batch mode, just put it
    in C:\Windows\System32.
    
  Overview:
  
    If you installed grabpng correctly you can click the icon or
    type "grabpng" at the command line to use the visual editor.
    
    If you are using grabpng from the command line you have
    powerful batch options available to you:

  Options:

    -grab <x> <y>     Set contents of grAb chunk in files.
    -alph             Create alPh chunk in files.
    -noalph           Remove alPh chunk from files.

  Notes:

    The <x> and <y> -grab values can be expressions.
    
    Variables allowed in expressions are:
      w - image width
      h - image height
    
    Constants allowed in expressions are:
      W - 320
      H - 200
      
  Keyboard Shortcuts:

    Ctrl-S    Save current image
    Z         Load previous image in directory
    X         Load next image in directory
    L         Lock guides
    Tab       Select next control
    Arrows    Select controls
    Space     Activate selected control
    Esc       Quit program

  Examples:

    # Load the visual editor.
    
    grabpng

    # Load mymonster.png in the visual editor.
    
    grabpng mymonster.png

    # Grab two pngs at their centers and set alPh chunks.
    
    grabpng -grab w/2 h/2 -alph mydecal.png mydecal2.png

    # Grab all pngs in the current directory at their horizontal 
    # center, 4 pixels from the bottom of the image.
    
    grabpng -grab w/2 h-4 *.png
